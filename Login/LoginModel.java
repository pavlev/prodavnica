package Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dbUtil.dbConnection;
public class LoginModel {
    Connection connection;

    public LoginModel(){
        try{
            this.connection = dbConnection.getConnection();  //provera da li je povezano na bazu
        }
        catch
            (SQLException ex){
            ex.printStackTrace();
        }
        if(this.connection == null){
            System.exit(1);
        }
    }
    public boolean isDatabaseConnected(){
        return this.connection != null;
    }

    public boolean isLogin(String ime, String lozinka, String opt) throws Exception{
        PreparedStatement pr = null;
        ResultSet rs = null;
        String sql = "SELECT * from administrator where ime = ? and lozinka = ? and role = ?";
        try {
            pr = this.connection.prepareStatement(sql);
            pr.setString(1, ime);
            pr.setString(2, lozinka);
            pr.setString(3, opt);
            rs = pr.executeQuery();

            boolean boll1;
            if (rs.next()) {
                return true;
            }
            return false;
        }
            catch (SQLException ex ) {
                return false;
            }
            finally{ // mora da se zatvori konekcija
                pr.close();
                rs.close();
            }

    }
}
